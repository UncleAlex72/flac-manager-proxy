FROM nginx:alpine

MAINTAINER Alex Jones <alex.jones@unclealex.co.uk>

COPY nginx.conf /etc/nginx/conf.d/default.conf

